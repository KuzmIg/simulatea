import javafx.event.{ActionEvent, EventHandler}
import javafx.scene.layout.StackPane

import scala.collection.mutable
import scala.util.Random
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Insets
import scalafx.geometry.Insets
import scalafx.scene.{layout, Scene}
import scalafx.scene.chart.{XYChart, LineChart, NumberAxis}
import scalafx.scene.control._
import scalafx.scene.layout.{LayoutIncludes, VBox, HBox, BorderPane}

/**
 * @author Igor Kuzminykh
 */
object GUI extends JFXApp with SimulatedAnnealing {

  private val xAxis = new NumberAxis
  private val yAxis = new NumberAxis
  private val lineChart = LineChart(xAxis, yAxis)
  lineChart.title = "Simulate"
  lineChart.createSymbols = false


  private val initialTemper: TextField = new TextField(){
    text = "15"
  }
  private val endTemper: TextField = new TextField(){
    text = "0.01"
  }

  private val ts: GRAP = new GRAP()
  var pane2sfx1: layout.StackPane = LayoutIncludes.jfxStackPane2sfx(ts.getStackPane)
  private val xs: Button = new Button("start") {
    onAction = new EventHandler[ActionEvent] {
      override def handle(event: ActionEvent): Unit = {
        initialTemperature = parseDouble(initialTemper.text.getValueSafe).getOrElse(initialTemperature)
        endTemperature = parseDouble(endTemper.text.getValueSafe).getOrElse(endTemperature)
        val algorithm: (mutable.MutableList[(Double, Double)], Double, Double, Candidate) = startLearn
        maxValue.text = algorithm._2.toString
        iterations.text = algorithm._3.toInt.toString
        firstParam.text = algorithm._4.x1.toString
        secondParam.text = algorithm._4.x2.toString
        val data = ObservableBuffer(algorithm._1 map { case (x, y) => XYChart.Data[Number, Number](x, y) })
        val series = XYChart.Series[Number, Number]("max", data)
        lineChart.getData.add(series)
        ts.getPlot.remove()
        ts.getPlot.drawPoint(algorithm._4.x1, algorithm._4.x2)
      }
    }
  }


  private val leftPart: HBox = new HBox() {
    padding = Insets(10, 0, 10, 10)
    spacing = 40

    children = List(
      new VBox() {
        spacing = 15
        children = List(
          new Label("Initial Temper"),
          new Label("End Temper")
        )
      },
      new VBox() {
        spacing = 10
        children = List(
          initialTemper,
          endTemper
        )

      },
      xs
    )
  }


  val maxValue: Label = new Label()
  val iterations: Label = new Label()
  val firstParam: Label = new Label()
  val secondParam: Label = new Label()

  private val statee: HBox = new HBox() {
    padding = Insets(10)
    spacing = 10
    children = List(
      new VBox() {
        children = List(
          new HBox() {
            children = List(new Label("Energy: "),
              maxValue)
          },
          new HBox() {
            children = List(new Label("x1: "),
              firstParam)
          },
          new HBox() {
            children = List(new Label("x2: "),
              secondParam)
          }
        )
      },
      new Label("Iterations: "),
      iterations
    )
  }

  private val ll: SplitPane = new SplitPane() {
    padding = Insets(0, 10, 0, 0)
  }
  ll.items.add(leftPart)


  private val tabPanel: TabPane = new TabPane() {
    tabs = List(
      new Tab() {
        text = "Max"
        content = lineChart
      },
      new Tab() {
        text = "Sjow"
        content = pane2sfx1
      }
    )
  }


  private val pane: BorderPane = new BorderPane() {
    left = ll
    center = tabPanel
    bottom = statee
  }
  stage = new PrimaryStage {
    title = "Simulate"
    scene = new Scene(800, 600) {
      root = pane
    }
  }

  def parseDouble(s: String) = try {
    Some(s.toDouble)
  } catch {
    case _: Exception => None
  }


}
