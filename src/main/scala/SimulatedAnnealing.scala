import scala.collection.mutable
import scala.util.Random

/**
 * @author Igor Kuzminykh
 */
trait SimulatedAnnealing {
  var initialTemperature = 15d
  var endTemperature = 0.01

  def startLearn: (mutable.MutableList[(Double, Double)], Double, Double, Candidate) = {
    val values: mutable.MutableList[(Double, Double)] = new mutable.MutableList[(Double, Double)]
    var T = initialTemperature
    var state: Candidate = generateCandidate(new Candidate(0, 0), T)
    var energy: Double = calcEnergy(state)
    var k: Double = 1
    while (T > endTemperature) {
      val currentState: Candidate = generateCandidate(state, T)
      val currentEnergy: Double = calcEnergy(currentState)
      if (energy < currentEnergy || makeTransit(Math.exp(-(energy - currentEnergy) / T))) {
        energy = currentEnergy
        state = currentState
      }
      val tuple: (Double, Double) = (k, energy)
      values += tuple
      T = initialTemperature * 0.1 / k
//      T = initialTemperature / Math.log(k)
      k += 1
    }
    (values, energy, k, state)
  }


  def ODZ(x1: Double, x2: Double): Boolean = {
    UTIL.odsA(x1, x2) && UTIL.odsB(x1, x2) && UTIL.odsC(x1, x2)
  }

  def calcEnergy(x: Candidate): Double = {
    UTIL.MAX(x.x1, x.x2)
  }

  def generateCandidate(xx: Candidate, t: Double): Candidate = {
    var gaussian1: Double = Gaussian.gaussian(xx.x1, t)
    var gaussian2: Double = Gaussian.gaussian(xx.x2, t)
    while (!ODZ(gaussian1, gaussian2)) {
      gaussian1 = Gaussian.gaussian(xx.x1, t)
      gaussian2 = Gaussian.gaussian(xx.x2, t)
    }
    new Candidate(gaussian1, gaussian2)
  }


  def makeTransit(probability: Double): Boolean = {
    if (probability > 1 || probability < 0)
      sys.error("Violation of argument constraint")
    val value: Double = new Random().nextDouble()
    value <= probability
  }

  case class Candidate(x1: Double, x2: Double)

}
