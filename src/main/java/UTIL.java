/**
 * @author Igor Kuzminykh
 */
public class UTIL {

  public static boolean odsB(double x1, double x2) {
    boolean c4 = x1 - 5d * x2 - Math.max(x1, 2d * x2) * 0.6 <= 1.5;
//    boolean c4 = x1 - 5d * x2 - Math.max(x1, 2d * x2) * 0.6 <= 10;
    boolean c5 = x1 - 5d * x2 + Math.max(x1, 2d * x2) * 0.6 >= -1.5;
//    boolean c5 = x1 - 5d * x2 + Math.max(x1, 2d * x2) * 0.6 >= -3;
    return c4 && c5;
  }

  public static boolean odsA(double x1, double x2) {
    boolean c2 = 8d * x1 - 2d * x2 - Math.max((3d / 2d) * x1, (1d / 2d) * x2) * 0.7 <= 16.45;
    boolean c3 = 8d * x1 - 2d * x2 + Math.max((3d / 2d) * x1, (1d / 2d) * x2) * 0.7 >= 11.55;
    return c2 && c3;
  }

  public static boolean odsC(double x1, double x2) {
    return Math.max(x1, (3d / 2d) * x2) >= -5;
  }


  public static double MAX(double x1, double x2) {
    return x1 + 5d * x2 + (1 / 2d) * Math.max(x1, (3 / 2d) * x2);
  }

}
