import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;

import java.util.function.BiFunction;

// Java 8 code
public class GRAP extends Application {

  public GRAP() {
    Axes axes = new Axes(1024, 700, -5, 5, 0.5, -5, 5, 0.2);
    plota = new Plot(axes);
  }

  public Plot getPlot() {
    return plota;
  }

  private Plot plota;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(final Stage stage) {
    StackPane layout = getStackPane();
    stage.setTitle("Нанана");
    stage.setScene(new Scene(layout, Color.rgb(35, 39, 50)));
    stage.show();
  }

  public StackPane getStackPane() {
    StackPane layout = new StackPane(plota);
    layout.setPadding(new Insets(20));
    layout.setStyle("-fx-background-color: rgb(35, 39, 50);");
    return layout;
  }

  class Axes extends Pane {
    private NumberAxis xAxis;
    private NumberAxis yAxis;

    public Axes(
            int width, int height,
            double xLow, double xHi, double xTickUnit,
            double yLow, double yHi, double yTickUnit
    ) {
      setMinSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
      setPrefSize(width, height);
      setMaxSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);

      xAxis = new NumberAxis(xLow, xHi, xTickUnit);
      xAxis.setSide(Side.BOTTOM);
      xAxis.setMinorTickVisible(false);
      xAxis.setPrefWidth(width);
      xAxis.setLayoutY(height / 2);

      yAxis = new NumberAxis(yLow, yHi, yTickUnit);
      yAxis.setSide(Side.LEFT);
      yAxis.setMinorTickVisible(false);
      yAxis.setPrefHeight(height);
      yAxis.layoutXProperty().bind(Bindings.subtract((width / 2) + 1, yAxis.widthProperty()));

      getChildren().setAll(xAxis, yAxis);
    }

    public NumberAxis getXAxis() {
      return xAxis;
    }

    public NumberAxis getYAxis() {
      return yAxis;
    }
  }

  class Plot extends Pane {
    private final Axes axes;

    public Plot(Axes axes) {
      this.axes = axes;
      BiFunction<Double, Double, Boolean> odsA = UTIL::odsA;
      Color color1 = Color.ORANGE.deriveColor(0, 1, 1, 0.6);
      BiFunction<Double, Double, Boolean> odsB = UTIL::odsB;
      Color color2 = Color.BLUE.deriveColor(0, 1, 1, 0.6);
      Path path1 = getPath(axes, color1, odsA);
      Path path2 = getPath(axes, color2, odsB);
      getChildren().add(0, axes);
      getChildren().add(1, path1);
      getChildren().add(2, path2);

    }

    private Path getPath(Axes axes, Color color, BiFunction<Double, Double, Boolean> ods) {
      Path path = new Path();
      path.setStroke(color);
      path.setStrokeWidth(5);
      path.setClip(new Rectangle(0, 0, axes.getPrefWidth(), axes.getPrefHeight()));
      boolean first = true;
      for (double xx = -8; xx < 8; xx += 0.01)
        for (double yy = -8; yy < 8; yy += 0.01) {
          if (ods.apply(xx, yy)) {
            if (first) {
              path.getElements().add(new MoveTo(mapX(xx, axes), mapY(yy, axes)));
              first = false;
            }
            path.getElements().add(new LineTo(mapX(xx, axes), mapY(yy, axes)));
          }
        }
      setMinSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
      setPrefSize(axes.getPrefWidth(), axes.getPrefHeight());
      setMaxSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
      return path;
    }

    private double mapX(double x, Axes axes) {
      double tx = axes.getPrefWidth() / 2;
      double sx = axes.getPrefWidth() / (axes.getXAxis().getUpperBound() - axes.getXAxis().getLowerBound());
      return x * sx + tx;
    }

    private double mapY(double y, Axes axes) {
      double ty = axes.getPrefHeight() / 2;
      double sy = axes.getPrefHeight() / (axes.getYAxis().getUpperBound() - axes.getYAxis().getLowerBound());
      return -y * sy + ty;
    }

    public void drawPoint(double x1, double x2) {
      Path pathPoint = new Path();
      pathPoint.setStroke(Color.RED);
      pathPoint.setStrokeWidth(5);
      pathPoint.setClip(new Rectangle(0, 0, axes.getPrefWidth(), axes.getPrefHeight()));
      pathPoint.getElements().add(new MoveTo(mapX(x1, axes), mapY(x2, axes)));
      pathPoint.getElements().add(new LineTo(mapX(x1, axes), mapY(x2, axes)));
      getChildren().add(3, pathPoint);
    }

    public void remove() {
      if (plota.getChildren().size() == 4)
        plota.getChildren().remove(3);
    }

  }
}